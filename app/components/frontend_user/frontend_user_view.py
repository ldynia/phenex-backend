import app
from flask import flash
from flask import url_for
from flask import request
from flask import redirect
from flask import render_template
from flask_login import login_required
from flask_classful import route
from flask_classful import FlaskView
from app.base.form.address_form import AddressForm
from app.components.frontend_user.frontend_user_form import FrontendUserAddForm
from app.components.frontend_user.frontend_user_form import FrontendUserEditForm
from app.components.frontend_user.frontend_user_form import FrontendUserDeleteForm
from app.components.frontend_user.frontend_user_repository import FrontendUserRepository


class FrontendUserView(FlaskView):
    view = 'FrontendUserView'
    metadata_view = 'FrontendUserMetadataView'
    title = 'Frontend Users'
    route_base = '/frontend_users'
    trailing_slash = False

    @login_required
    @route('/', methods=['GET'], strict_slashes=False)
    def index(self):
        page = int(request.args.get('page', 1))
        context = {
            'view': self.view,
            'metadata_view': self.metadata_view,
            'title': self.title,
            'pagination': FrontendUserRepository().paginate(page=page),
            'form_delete': FrontendUserDeleteForm(id=id)
        }

        return render_template('frontend_user_index.html', **context)

    @login_required
    def get(self, id):
        user = FrontendUserRepository().find_one(id=id)
        context = {
            'item': user,
            'title': self.title
        }

        return render_template('frontend_user_get.html', **context)

    @login_required
    @route('add', methods=['GET', 'POST'])
    def add(self):
        form = FrontendUserAddForm(request.form)
        address_form = AddressForm(request.form)
        context = {
            'form': form,
            'view': self.view,
            'title': self.title,
        }
        if request.method == 'GET':
            return render_template('frontend_user_add.html', **context)

        if request.method == 'POST':
            if form.validate() is False:
                return render_template('frontend_user_add.html', **context)

            form.clear()

            user = FrontendUserRepository().create_model(**form.data)
            user.set_password(form.data['password'])
            if not user.save():
                flash("Failed to create user", "error")
            else:
                flash("User was created", "success")

            return redirect(url_for(self.view + ':index'))

    @login_required
    @route('<id>/edit', methods=['GET', 'POST'])
    def edit(self, id):
        user = FrontendUserRepository().find_one(id=id)
        form = FrontendUserEditForm(request.form, obj=user)
        context = {
            'item': user,
            'form': form,
            'view': self.view,
            'title': self.title,
        }

        if request.method == 'GET':
            return render_template('frontend_user_edit.html', **context)

        if request.method == 'POST':
            if id != request.form['id']:
                flash("Invalid request due ids missmatch", "error")
                return redirect(url_for(self.view + ':edit', id=id))

            if form.validate() is False:
                print('gggg', form.errors)
                flash("Failed to update user", "error")
                return render_template('frontend_user_edit.html', **context)

            form.clear()

            if not user.update(**form.data):
                flash("Failed to update user", "error")
            else:
                flash("User was updated", "success")

            return redirect(url_for(self.view + ':edit', id=id))

    @login_required
    @route('<id>/delete', methods=['POST'])
    def delete(self, id):
        delet_form = FrontendUserDeleteForm(request.form)
        if delet_form.validate() is False:
            flash("Delete action stoped by form validation", "error")
            return redirect(url_for(self.view + ':index'))

        user = FrontendUserRepository().find_one(id=id)
        if user.delete() is None:
            flash("User has been deleted", "success")
        else:
            flash("Failed to delete user", "error")

        return redirect(url_for(self.view + ':index'))
