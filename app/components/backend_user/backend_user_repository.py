from app.utilities.repository import Repository
from app.components.backend_user.backend_user_model import BackendUser


class BackendUserRepository(Repository):

    def __init__(self):
        # Invoking base constructor
        Repository.__init__(self, BackendUser)
