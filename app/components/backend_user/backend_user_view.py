import app
from flask import flash
from flask import url_for
from flask import request
from flask import redirect
from flask import render_template
from flask_login import login_required
from flask_classful import route
from flask_classful import FlaskView
from app.components.backend_user.backend_user_repository import BackendUserRepository
from app.components.backend_user.backend_user_form import BackendUserAddForm
from app.components.backend_user.backend_user_form import BackendUserEditForm
from app.components.backend_user.backend_user_form import BackendUserDeleteForm


class BackendUserView(FlaskView):
    view = 'BackendUserView'
    title = 'Backend User'
    route_base = '/backend_users'
    trailing_slash = False

    @login_required
    @route('/', methods=['GET'], strict_slashes=False)
    def index(self):
        page = int(request.args.get('page', 1))
        context = {
            'view': self.view,
            'title': self.title,
            'pagination': BackendUserRepository().paginate(page=page),
            'form_delete': BackendUserDeleteForm(id=id)
        }

        return render_template('backend_user_index.html', **context)

    @login_required
    def get(self, id):
        user = BackendUserRepository().find_one(id=id)
        context = {
            'item': user,
            'title': self.title
        }

        return render_template('backend_user_get.html', **context)

    @login_required
    @route('add', methods=['GET', 'POST'])
    def add(self):
        form = BackendUserAddForm(request.form)
        context = {
            'form': form,
            'view': self.view,
            'title': self.title
        }
        if request.method == 'GET':
            return render_template('backend_user_add.html', **context)

        if request.method == 'POST':
            if form.validate() is False:
                return render_template('backend_user_add.html', **context)

            form.clear()

            user = BackendUserRepository().create_model(**form.data)
            user.set_password(form.data['password'])
            if not user.save():
                flash("Failed to create user", "error")
            else:
                flash("User was created", "success")

            return redirect(url_for(self.view + ':index'))

    @login_required
    @route('<id>/edit', methods=['GET', 'POST'])
    def edit(self, id):
        user = BackendUserRepository().find_one(id=id)
        form = BackendUserEditForm(request.form, obj=user)
        context = {
            'item': user,
            'form': form,
            'view': self.view,
            'title': self.title
        }

        if request.method == 'GET':
            return render_template('backend_user_edit.html', **context)

        if request.method == 'POST':
            if id != request.form['id']:
                flash("Invalid request due to 'ids' missmatch", "error")
                return redirect(url_for(self.view + ':edit', id=id))

            if form.validate() is False:
                return render_template('backend_user_edit.html', **context)

            form.clear()

            if not user.update(**form.data):
                flash("Failed to update user", "error")

            flash("User was updated", "success")

            return redirect(url_for(self.view + ':edit', id=id))

    @login_required
    @route('<id>/delete', methods=['POST'])
    def delete(self, id):
        delet_form = BackendUserDeleteForm(request.form)
        if delet_form.validate() is False:
            flash("Delete action stoped by form validation", "error")
            return redirect(url_for(self.view + ':index'))

        user = BackendUserRepository().find_one(id=id)
        if user.delete() is None:
            flash("User has been deleted", "success")
        else:
            flash("Failed to delete user", "error")

        return redirect(url_for(self.view + ':index'))
