from app import mongo
from app import bcrypt
from datetime import datetime
from app.base.model.metadata import Metadata

Document = mongo.Document
IntField = mongo.IntField
EmailField = mongo.EmailField
StringField = mongo.StringField
BooleanField = mongo.BooleanField
DateTimeField = mongo.DateTimeField
EmbeddedDocumentField = mongo.EmbeddedDocumentField


# Read: https://flask-login.readthedocs.io/en/latest/#your-user-class
class BackendUser(Document):
    email = EmailField(required=True, unique=True)
    username = StringField(required=True, max_length=16)
    password = StringField(required=True, max_length=60)
    active = BooleanField(default=False)
    verified = BooleanField(default=False)
    access_level = IntField(default=0, min=0, max=10)
    login_failed_count = IntField(default=0, min=0, max=3)
    login_ban_expires_at = DateTimeField()
    metadata = EmbeddedDocumentField(Metadata, default=Metadata())
    created_at = DateTimeField(default=datetime.utcnow())
    modified_at = DateTimeField(default=datetime.utcnow())

    blacklisted_fields = ['password']

    def __init__(self, *args, **kwargs):
        super(Document, self).__init__(*args, **kwargs)

    meta = {
        'strict': True,
        'collection': 'backend_users'
    }

    # before save
    def save(self, *args, **kwargs):
        if not self.created_at:
            self.created_at = datetime.utcnow()
        self.modified_at = datetime.utcnow()

        return super(BackendUser, self).save(*args, **kwargs)

    def get_id(self):
        return str(self.id)

    def is_active(self):
        return self.active

    def is_anonymous(self):
        return False

    def is_authenticated(self):
        return self.authenticated

    def is_valid_password(self, password_candidate):
        return bcrypt.check_password_hash(self.password, password_candidate)

    def set_password(self, password):
        self.password = self.__encrypt_password(password)

    def __encrypt_password(self, password):
        return str(bcrypt.generate_password_hash(password), 'utf-8')
