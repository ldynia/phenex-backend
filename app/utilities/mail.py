from app import mail
from flask import render_template
from flask_mail import Message
from app.constants import MAIL_SENDER


class Mail:

    def send(self, subject, recipients, template, **context):
        message = Message(subject=subject, recipients=recipients, sender=MAIL_SENDER)
        message.html = render_template(template, **context)
        # TODO figureout what to do with email when failed to send them
        try:
            mail.send(message)
            return True
        except Exception as e:
            return False
