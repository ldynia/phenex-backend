

class MetadataFormHandler():

    form = None
    context = None
    active_form = None

    def __init__(self, form, context):
        self.form = form
        self.context = context

        if 'id' in context:
            self.context['form_address_home'] = form(id=self.context['id'])
            self.context['form_address_work'] = form(id=self.context['id'])
            self.context['form_address_other'] = form(id=self.context['id'])

    def extract(self, form=None, from_context=False, **kwargs):
        if from_context:
            if 'metadata' in self.context and 'home' in self.context['metadata']['address']:
                self.context['form_address_home'] = self.form(id=self.context['id'], obj=self.context['metadata']['address']['home'])

            if 'metadata' in self.context and 'work' in self.context['metadata']['address']:
                self.context['form_address_work'] = self.form(id=self.context['id'], obj=self.context['metadata']['address']['work'])

            if 'metadata' in self.context and 'other' in self.context['metadata']['address']:
                self.context['form_address_other'] = self.form(id=self.context['id'], obj=self.context['metadata']['address']['other'])

            self.active_form = self.form(form)

        if not from_context:
            if form.get('metadata') == 'address' and form.get('type') == 'home':
                self.active_form = self.form(form)
                self.context['form_address_home'] = self.active_form

            if form.get('metadata') == 'address' and form.get('type') == 'work':
                self.active_form = self.form(form)
                self.context['form_address_work'] = self.active_form

            if form.get('metadata') == 'address' and form.get('type') == 'other':
                self.active_form = self.form(form)
                self.context['form_address_other'] = self.active_form

        return self.context, self.active_form

    def bind(self, user, form):
        if form.data['metadata'] and 'address' in form.data['metadata']:
            user.metadata['address'][form.data['type']] = form.data2model()
        return user
