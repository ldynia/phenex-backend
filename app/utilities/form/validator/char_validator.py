from wtforms import ValidationError


class CharValidator(object):
    """
    Validate ocurence of charaters in string
    """

    def __init__(self, char=' ', min=None, max=None, count=None, message=None):
        self.char = char
        self.min = min
        self.max = max
        self.count = count
        self.message = message

    def __call__(self, form, field):
        spaces_count = field.data.count(self.char)

        if (self.count):
            char_normalized = self.__char_normalize(self.char)
            message = u'Field must contain exactly %i " %s" character(s)' % (self.count, char_normalized)
            raise ValidationError(message)

        if (self.min and self.max) and (spaces_count < self.min or spaces_count > self.max):
            char_normalized = self.__char_normalize(self.char)
            message = u'Field must contain between  %i and %i " %s" character(s)' % (self.min, self.max, char_normalized)
            raise ValidationError(message)

        if (self.min and self.max is None) and (spaces_count < self.min):
            char_normalized = self.__char_normalize(self.char)
            message = u'Field must contain more than %i " %s" character(s)' % (self.min, char_normalized)
            raise ValidationError(message)

    def __char_normalize(self, char):
        if self.char == ' ':
            return 'white space'
        return char
