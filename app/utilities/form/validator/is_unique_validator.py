from wtforms import ValidationError


class IsUniqueValidator(object):

    def __init__(self, model, unique_to='all', message=None):
        self.model = model
        self.unique_to = unique_to
        self.message = message

    def __call__(self, form, field):
        data = {field.name: field.data}
        if self.unique_to == 'all':
            object = self.model.objects.filter(**data).first()
            if object:
                raise ValidationError(u'Provided %s already exist' % (field.name))
        else:
            # _nt -not equal
            data[self.unique_to + '__ne'] = form.data[self.unique_to]
            object = self.model.objects.filter(**data).first()
            if object is not None:
                raise ValidationError(u'Provided %s already exist' % (field.name))
