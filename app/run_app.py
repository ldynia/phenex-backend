"""
Run this file with uwsgi
$ cd /srv/www/htdocs/config/services
$ uwsgi --ini app.uwsgi.development.ini --http-socket /var/run/app.uwsgi.sock
"""
from app import create_app
from app.constants import APP_HOST
from app.constants import APP_THREADED
from app.components.admin.admin_view import AdminView
from app.components.health.health_view import HealthView
from app.components.backend_user.backend_user_view import BackendUserView
from app.components.frontend_user.frontend_user_view import FrontendUserView
from app.components.frontend_user.frontend_user_metadata_view import FrontendUserMetadataView

app = create_app()

# Import template utilities
from app.utilities.template import *

# link: https://stackoverflow.com/questions/34615743/unable-to-load-configuration-from-uwsgi#answer-37175998
if __name__ == "__main__":
    app.run(host=APP_HOST, threaded=APP_THREADED)
