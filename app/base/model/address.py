from app import mongo
from wtforms import HiddenField
from wtforms.validators import Regexp
from app.constants import REGEX
from app.constants import COUNTRIES
from app.utilities.form.validator.char_validator import CharValidator


StringField = mongo.StringField
EmbeddedDocument = mongo.EmbeddedDocument


class Address(EmbeddedDocument):
    street = StringField(default=None, required=True, min_length=6, max_length=255, validators=[
        CharValidator(char=' ', min=1),
        Regexp(regex=REGEX['street']['with_number'])
    ])
    zipcode = StringField(default=None, required=True, min_length=REGEX['zipcode_length']['Poland'], max_length=REGEX['zipcode_length']['Poland'], validators=[
        Regexp(regex=REGEX['zipcode']['Poland'])
    ])
    city = StringField(default=None, required=True, min_length=3, max_length=128)
    country = StringField(choices=COUNTRIES, default='Poland')
