import click
from app import app_cli

# from db.seed import Seed
# from app.components.backend_user.backend_user_form import BackendUserAddForm
# from app.components.backend_user.backend_user_repository import BackendUserRepository


@app_cli.command('seed', help="Populate database with dummy data.")
def seed():
    click.echo('Start populating: database.')
    # Seed.create_backend_user()
    # Seed.create_frontend_user()
    click.echo('Stopped populating: database.')
