from wtforms import Form
from wtforms import HiddenField
from wtforms.validators import DataRequired
from app.base.model.address import Address
from app.utilities.form.validator.choice_validator import ChoiceValidator
from app.constants import ALLOWED_METADATA
from app.constants import ALLOWED_ADDRESSES
from flask_wtf import FlaskForm
from flask_mongoengine.wtf import model_form

AddressBaseForm = model_form(Address)


class AddressForm(AddressBaseForm):
    id = HiddenField('Id', [
        DataRequired()
    ])
    type = HiddenField('Type', [
        DataRequired(),
        ChoiceValidator(ALLOWED_ADDRESSES)
    ])
    metadata = HiddenField('Metadata', [
        DataRequired(),
        ChoiceValidator(ALLOWED_METADATA)
    ], default='address')

    class Meta:
        csrf = True

    def data2model(self):
        return Address(
            street=self.street.data,
            zipcode=self.zipcode.data,
            city=self.city.data,
            country=self.country.data
        )


class AddressDeleteForm(FlaskForm):
    id = HiddenField('Id', [
        DataRequired()
    ])
    type = HiddenField('Type', [
        DataRequired(),
        ChoiceValidator(ALLOWED_ADDRESSES)
    ])
    metadata = HiddenField('Metadata', [
        DataRequired(),
        ChoiceValidator(ALLOWED_METADATA)
    ], default='address')
    method = HiddenField('Method', [
        DataRequired(),
        ChoiceValidator(['DELETE'])
    ], default='DELETE')

    class Meta:
        csrf = True
