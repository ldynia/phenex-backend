# Dockerfile

```bash
# Build images
$ docker-compose -f composer-development.yml build
$ docker-compose -f composer-testing.yml build
$ docker-compose -f composer-production.yml build

# Run project
$ docker-compose -f composer-development.yml up
$ docker-compose -f composer-testing.yml up
$ docker-compose -f composer-production.yml up
```

```bash
# Development
$ docker build  --build-arg BUILD_DATE=$(date -u +'%d-%m-%YT%H:%M:%S') \
  --build-arg GIT_COMMIT=0 \
  --build-arg DEPLOYMENT=development \
  --build-arg DEBUG=true \
  -t genomicepidemiology/phenex-backend:dev \
  -f docker/Dockerfile.development .

# Testing
$ docker build  --build-arg BUILD_DATE=$(date -u +'%d-%m-%YT%H:%M:%S') \
  --build-arg GIT_COMMIT=0 \
  --build-arg DEPLOYMENT=testing \
  --build-arg DEBUG=true \
  -t genomicepidemiology/phenex-backend:test \
  -f docker/Dockerfile.testing .

# Production
$ docker build  --build-arg BUILD_DATE=$(date -u +'%d-%m-%YT%H:%M:%S') \
  --build-arg GIT_COMMIT=0 \
  --build-arg DEPLOYMENT=production \
  --build-arg DEBUG=true \
  -t genomicepidemiology/phenex-backend:prod \
  -f docker/Dockerfile.production .
```

```bash
$ docker run --name phenex_backend --rm -d genomicepidemiology/phenex-backend:prod
 ```

# Mongo

## Backup and Restore
```bash
# inside mongo container
$ mongodump --db phenex_dev --out /data/db/backup/fixtures
$ mongorestore /data/db/backup/fixtures
```
# Testing

## Selenium

```bash
$ selenium-side-runner --timeout 2000 -s http://chromedriver:4444 tests/e2e/selenium/*.side
```
