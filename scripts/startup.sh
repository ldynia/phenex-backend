#!/usr/bin/env ash

# Set timezone
echo "Setting up timezone"
rm -f /etc/localtime
echo $TZ > /etc/timezone

# Remove precompiled files
echo "Deleting *.pyo, *.pyc and __pycache__"
find /src -type f -name *.pyo -delete
find /src -type f -name *.pyc -delete
find /src -type d -name __pycache__ -delete

if [ "$DEPLOYMENT" = "development" ]; then
  yarn install
  pip3 install --upgrade pip
  pip3 install -r requirements.txt

  ## SUPERVISOR CONFIG
  ln -s /src/scripts/aliases.sh /etc/profile.d/aliases.sh
  ln -s /src/config/services/supervisor.${DEPLOYMENT}.ini /etc/supervisor.d/supervisor.ini

  gulp --gulpfile scripts/gulpfile.js --${DEPLOYMENT}

  /usr/bin/supervisord -j /run/supervisord.pid

  # infinity sleep
  sh -c 'while sleep 3600; do :; done'
fi
