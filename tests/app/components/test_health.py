import pytest


ready_endpoint = '/health/ready'
alive_endpoint = '/health/alive'


def test_ready_endpoint(app, client):
    response = client.get(ready_endpoint)

    assert response.status_code == 200


def test_alive_endpoint(app, client):
    response = client.get(alive_endpoint)

    assert response.status_code == 200
