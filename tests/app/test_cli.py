import pytest

from app.base.commands.seed import seed
from app.components.backend_user.backend_user_commands import create_admin


# base commands
def test_seed(runner):
    result = runner.invoke(seed)

    assert 'Start populating' in result.output
    assert 'Stopped populating' in result.output


# backend user commands
def test_create_admin(app, runner):
    args = [
        '--email', app.config['TESTING_EMAIL'],
        '--username', app.config['TESTING_EMAIL'].split('@')[0],
        '--password', app.config['TESTING_PASSWORD'],
        '--password_confirm', app.config['TESTING_PASSWORD'],
        '--access_level', 0,
    ]
    result = runner.invoke(create_admin, args)

    assert 'Admin created' in result.output
