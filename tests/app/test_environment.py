import os
import pytest
from app.constants import HOST
from app.constants import FLASK_ENV
from app.constants import APP_DEBUG
from app.constants import APP_SECRET
from app.constants import APP_TIMEZONE
from app.constants import DEPLOYMENT
from app.constants import MONGO_DB_HOST
from app.constants import MONGO_DB_PORT
from app.constants import MONGO_DB_NAME
from app.constants import TESTING_EMAIL
from app.constants import TESTING_PASSWORD
from app.constants import TESTING_MONGO_DB_NAME
from app.constants import MIN_CODE_COVERAGE


def test_envars(app):
    assert_deployment()
    assert_debug(DEPLOYMENT)
    assert_flask(DEPLOYMENT)
    assert_testing(DEPLOYMENT)
    assert_secret()
    assert_host()
    assert_mongo(DEPLOYMENT)
    assert_timezone()
    assert_code_coverage()
    assert_files(app, DEPLOYMENT)


def assert_files(app, deployment):
    if deployment.startswith('dev'):
        assert os.path.exists(app.config['APP_ROOT'] + '/.env.secrets')


def assert_flask(deployment):
    assert FLASK_ENV is not None

    if deployment.startswith('dev'):
        assert FLASK_ENV == 'development'

    if deployment.startswith('prod'):
        assert FLASK_ENV == 'production'


def assert_secret():
    # 2x256-bit WEP Keys from https://randomkeygen.com/
    assert APP_SECRET is not None
    assert len(APP_SECRET) == 58


def assert_deployment():
    assert DEPLOYMENT is not None
    assert DEPLOYMENT.startswith(('prod', 'dev', 'test'))


def assert_host():
    assert HOST is not None
    # such .com
    assert '.' in HOST


def assert_debug(deployment):
    assert APP_DEBUG is not None

    if deployment.startswith(('dev', 'test')):
        assert APP_DEBUG is True
    else:
        assert APP_DEBUG is False


def assert_testing(deployment):
    assert TESTING_EMAIL is not None
    assert TESTING_PASSWORD is not None
    assert TESTING_MONGO_DB_NAME is not None

    if deployment.startswith(('dev', 'test')):
        assert TESTING_MONGO_DB_NAME.startswith(MONGO_DB_NAME)
        assert TESTING_MONGO_DB_NAME.endswith('test')


def assert_mongo(deployment):
    assert MONGO_DB_HOST is not None
    assert MONGO_DB_NAME is not None
    assert MONGO_DB_PORT == 27017


def assert_code_coverage():
    assert MIN_CODE_COVERAGE is not None
    assert isinstance(MIN_CODE_COVERAGE, int)


def assert_timezone():
    assert APP_TIMEZONE is not None
    assert APP_TIMEZONE.startswith('Europe')
    assert APP_TIMEZONE.endswith('Copenhagen')
