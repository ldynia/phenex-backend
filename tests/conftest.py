import pytest

from app import mongo
from app import create_app
from app.constants import TESTING_EMAIL
from app.constants import TESTING_PASSWORD
from app.constants import TESTING_MONGO_DB_NAME


@pytest.fixture
def app():

    app = create_app({
        'DEBUG': True,
        'TESTING': True,
        'TESTING_EMAIL': TESTING_EMAIL,
        'TESTING_PASSWORD': TESTING_PASSWORD,
        'MONGODB_DB': TESTING_MONGO_DB_NAME,
    })

    with app.app_context():
        delete_database(app)

    return app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()


def delete_database(app):
    mongo.connection.drop_database(app.config['MONGODB_DB'])
