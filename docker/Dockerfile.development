FROM node:12.6.0-alpine AS build-static

ARG DEBUG=false
ARG VERSION=0.0.0
ARG DEPLOYMENT=production
ARG MAINTANER="ludd@food.dtu.dk"
ARG TZ
ENV TZ=Europe/Copenhagen
ARG DEBUG
ENV DEBUG=$DEBUG
ARG DEPLOYMENT
ENV DEPLOYMENT=$DEPLOYMENT
ARG FLASK_ENV=development
ENV FLASK_ENV=$FLASK_ENV
ARG VERSION
LABEL version=$VERSION
ARG GIT_COMMIT
LABEL git_commit=$GIT_COMMIT
ARG BUILD_BY
LABEL build_by=$BUILD_BY
ARG BUILD_DATE
LABEL build_date=$BUILD_DATE
ARG MAINTANER
LABEL maintainer=$MAINTANER

WORKDIR /src

COPY . ./

RUN mkdir -p /etc/supervisor.d/
RUN touch /run/supervisord.pid /run/supervisord.sock

# Install dependencies
RUN apk add --no-cache \
    build-base \
    curl \
    git \
    htop \
    iputils \
    libffi-dev \
    linux-headers \
    mc \
    python3-dev \
    supervisor \
    tig \
    vim \
    wget

RUN pip3 install --upgrade pip
RUN pip3 install uwsgi
RUN npm install -g \
  gulp \
  selenium-side-runner \
  yarn

EXPOSE 80
EXPOSE 443

VOLUME /src /src/storage

CMD sh /src/scripts/startup.sh
